import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    lawketBaseUrl: 'http://192.168.1.220:8080/lawket_php/',
    lawketImageUrl: 'http://192.168.1.220:8080/lawket_php/images/',
    ImageUrl: 'http://192.168.1.220:8080/images/',
    
    legalOfficeFlag: false, //for login with legalOffice
    legalOfficeData: [],    //for login with legalOffice content
    top_search: {
      search_text: '',      // search text for 통합검색 lo_company_name, lo_title, lo_core_business, lo_description
      search_mode: '',      // 'total' or 'detail' 
    },
    searchTextData: [],     //for searched text rank by user
    main_search: {
      region: '전체',        //search region for homePage, after navigate to SearchByRegion
      product: '전체',       //search product for homePage, after navigate to SearchByProduct
    },
    eachData: {
      noticeData: '',       //view detail for notice page
      informationData: '',  //view detail for information page
      customerData: '',     //view detail for customSearch page
      consultData: '',      //view detail for realTimeConsult page
    },
    today: {
      yyyymmdd: '',         //today format date --> yyyy-mm-dd
      yyyymm: '',           //this format month --> yyyy-mm
      yyyy: '',             //this format year  --> yyyy
      mm: '',               //this format month --> mm
      dd: '',               //this format day   --> dd
    },
  },
});