// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";

import App from "./App";
import router from "./router";

Vue.config.productionTip = false;

/* Install Bootstrap component for UI ---> $ npm i bootstrap-vue */
import BootstrapVue from "bootstrap-vue";
Vue.use(BootstrapVue);
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

/* Install es6-promise component for Internet Explorer v10, v11 ---> $ npm install es6-promise --save */
import "es6-promise/auto";


/* lawket -css */
import "./components/css/style.css"; // this file made by 김재원





/* Vue Carousel ---> $ Install Carousel for Slide component ---> npm install vue-carousel --save */
import VueCarousel from "vue-carousel"; //
Vue.use(VueCarousel);

/* Install axis component for REST API ---> $ npm install --save axios vue-axios */
import axios from "axios";
import VueAxios from "vue-axios";
Vue.use(VueAxios, axios);

/* Install vuex component for share data between components   ---> $ npm install vuex --save */
import Vuex from "vuex";
Vue.use(Vuex);
/* and make store component */
import { store } from "./store";

/* Install vue2-filters component for fomatter ex) 12,300   ---> $ npm install vue2-filters */
import Vue2Filters from "vue2-filters";
Vue.use(Vue2Filters);

/* Install vue-wysiwyg component for rich text editor ---> $ npm install vue-wysiwyg --save */
import wysiwyg from "vue-wysiwyg";
Vue.use(wysiwyg, {}); // config is optional. more below
import "vue-wysiwyg/dist/vueWysiwyg.css";




/* Install for hovermenu --->  $ npm install --save @progress/kendo-ui */
import "@progress/kendo-ui";
/* Install for hovermenu --->  $ npm install --save @progress/kendo-theme-default */
import "@progress/kendo-theme-default/dist/all.css";
/* Install for hovermenu --->  $ npm install --save @progress/kendo-layout-vue-wrapper */
import { Menu, LayoutInstaller } from "@progress/kendo-layout-vue-wrapper";
Vue.use(LayoutInstaller);

/* Install for Scroll to top --->  $ npm install --save vue-scrollto */
import VueScrollTo from 'vue-scrollto'
Vue.use(VueScrollTo);

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router, // for routing Add this line
  store, // for store Add this line
  components: { App },
  template: "<App/>"
});
