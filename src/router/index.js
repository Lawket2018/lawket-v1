import Vue from "vue";
import Router from "vue-router";
import Home from "@/components/Home";
import CustomSearch from "@/components/CustomSearch";
import SearchByRegion from "@/components/SearchByRegion";
import SearchByProduct from "@/components/SearchByProduct";
import RealTimeConsult from "@/components/RealTimeConsult";
import AddConsult from "@/components/AddConsult";
import Information from "@/components/Information";
import Questions from "@/components/Questions";
import AddQuestion from "@/components/AddQuestion";
import Tips from "@/components/Tips";
import UserGuide from "@/components/UserGuide";
import CustomerGuide from "@/components/CustomerGuide";
import Faq from "@/components/Faq";
import OneToOne from "@/components/OneToOne";
import Notice from "@/components/Notice";
import Notice2 from "@/components/Notice2";
import AdInformation from "@/components/AdInformation";
import RequestAd from "@/components/RequestAd";
import CompanyIntro from "@/components/CompanyIntro";
import Login from "@/components/Login";
import MyPage from "@/components/MyPage";
import SignUpCustomer from "@/components/SignUpCustomer";
import CreateCustomer from "@/components/CreateCustomer";
import UpdateCustomer from "@/components/UpdateCustomer";
import CreateLawyer from "@/components/CreateLawyer";
import Company_summary from "@/components/Company_summary";
import Company_history from "@/components/Company_history";
import Company_org from "@/components/Company_org";
import Company_vision from "@/components/Company_vision";
import how_use from "@/components/how_use";
/*
import SignUpUser from "@/components/SignUpUser";
import CreateUser from "@/components/CreateUser";
import UpdateUser from "@/components/UpdateUser";

*/

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/CustomSearch",
      name: "CustomSearch",
      component: CustomSearch
    },
    {
      path: "/SearchByRegion",
      name: "SearchByRegion",
      component: SearchByRegion
    },
    {
      path: "/SearchByProduct",
      name: "SearchByProduct",
      component: SearchByProduct
    },
    {
      path: "/RealTimeConsult",
      name: "RealTimeConsult",
      component: RealTimeConsult
    },
    {
      path: "/AddConsult",
      name: "AddConsult",
      component: AddConsult
    },
    {
      path: "/Information",
      name: "Information",
      component: Information
    },
    {
      path: "/Questions",
      name: "Questions",
      component: Questions
    },
    {
      path: "/AddQuestion",
      name: "AddQuestion",
      component: AddQuestion
    },
    {
      path: "/Tips",
      name: "Tips",
      component: Tips
    },
    {
      path: "/UserGuide",
      name: "UserGuide",
      component: UserGuide
    },
    {
      path: "/CustomerGuide",
      name: "CustomerGuide",
      component: CustomerGuide
    },

    {
      path: "/OneToOne",
      name: "OneToOne",
      component: OneToOne
    },
    {
      path: "/Faq",
      name: "Faq",
      component: Faq
    },
    {
      path: "/Notice",
      name: "Notice",
      component: Notice
    },
    {
      path: "/Notice2",
      name: "Notice2",
      component: Notice2
    },
    {
      path: "/AdInformation",
      name: "AdInformation",
      component: AdInformation
    },
    {
      path: "/RequestAd",
      name: "RequestAd",
      component: RequestAd
    },
    {
      path: "/CompanyIntro",
      name: "CompanyIntro",
      component: CompanyIntro
    },
    {
      path: "/Login",
      name: "Login",
      component: Login
    }, ,
    {
      path: "/MyPage",
      name: "MyPage",
      component: MyPage
    },
    {
      path: "/SignUpCustomer",
      name: "SignUpCustomer",
      component: SignUpCustomer
    },
    {
      path: "/CreateCustomer",
      name: "CreateCustomer",
      component: CreateCustomer
    },
    {
      path: "/CreateLawyer",
      name: "CreateLawyer",
      component: CreateLawyer
    },
    {
      path: "/UpdateCustomer",
      name: "UpdateCustomer",
      component: UpdateCustomer
    },
    {
      path: "/Company_summary",
      name: "Company_summary",
      component: Company_summary
    },
    {
      path: "/Company_history",
      name: "Company_history",
      component: Company_history
    },
    {
      path: "/Company_org",
      name: "Company_org",
      component: Company_org
    },
    {
      path: "/Company_vision",
      name: "Company_vision",
      component: Company_vision
    },
    {
      path: "/how_use",
      name: "how_use",
      component: how_use
    },
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
});
